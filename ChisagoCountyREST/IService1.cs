﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.ComponentModel;
using System.ServiceModel.Activation;
using System.Net.Http;
using System.IO;

namespace ChisagoCountyREST
{
    [ServiceContract]
    public interface IService
    {
        [Description("HTTP GET: Returns a document from the specified server/db matching the provided keywords (key/value pipe delimited -- e.g. CREATEUSER|JDOE|CREATEDATE|07312013)")]
        [WebInvoke(UriTemplate = "/?url={url}&u={u}&p={p}&db={db}&dt={dt}&keywords={keywords}",
            Method = "GET")]
        Stream GetDocumentGET(string url, string u, string p, string db, string dt, string keywords);

        [Description("HTTP POST: Returns a document from the specified server/db matching the provided keywords (supplied as JSON with header ContentType as application/json).")]
        [OperationContract]
        [WebInvoke(UriTemplate ="/", 
            BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            Method = "POST")]
        Stream GetDocumentPOST(string url, string u, string p, string db, string dt, string keywords);

    }
} 