﻿using Hyland.Types;
using Hyland.Unity;
using Hyland.Unity.PhysicalRecords;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace ChisagoCountyREST
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Service1 : IService
    {

        public Stream GetDocumentGET(string url, string u, string p, string db, string dt, string keywords)
        {
            return GetDocument(url, u, p, db, dt, keywords);
        }

        public Stream GetDocumentPOST(string url, string u, string p, string db, string dt, string keywords)
        {
            return GetDocument(url, u, p, db, dt, keywords); 
        }

        private Stream GetDocument(string url, string u, string p, string db, string dt, string keywords)
        {
            // Check if parameters are valid
            // Early return if the req query parameters haven't been supplied
            if (String.IsNullOrEmpty(url) || String.IsNullOrEmpty(u) || String.IsNullOrEmpty(p) || String.IsNullOrEmpty(db) || String.IsNullOrEmpty(dt) || String.IsNullOrEmpty(keywords))
            {
                return DisconnectAndReturnErrorStream(null, "Missing or Invalid Parameters", HttpStatusCode.BadRequest);
            }

            // Connect with the supplied url, db, username and password
            Application _app = null;
            try
            {
                AuthenticationProperties props = Application.CreateOnBaseAuthenticationProperties(url, u, p, db);
                //Uncomment for QueryMetering support
                props.LicenseType = LicenseType.QueryMetering;
                _app = Hyland.Unity.Application.Connect(props);
            }
            catch (Exception)
            {
                return DisconnectAndReturnErrorStream(_app, "Authentication Error", HttpStatusCode.Unauthorized);
            }
            // Check if connected, if not return a NoContent as set above
            if(!_app.IsConnected) 
            {
                return DisconnectAndReturnErrorStream(_app, "Authentication Error", HttpStatusCode.Unauthorized);
            }

            // Extract the keywords!
            string[] explodedKeywordKeyValues = keywords.Split('|');
            Dictionary<string, string> keywordMapping = new Dictionary<string, string>();
            for (int i = 1; i < explodedKeywordKeyValues.Length; i = i + 2)
            {
                keywordMapping.Add(explodedKeywordKeyValues[i - 1], explodedKeywordKeyValues[i]);
            }

            // Create the query
            DocumentQuery docQuery = _app.Core.CreateDocumentQuery();

            // Ensure the DocumentType exists (checking for both ID & Name) and add it to the query
            DocumentType docType = null;
            long dtId = 0;
            if (long.TryParse(dt, out dtId))
            {
                docType = _app.Core.DocumentTypes.Find(dtId);
            }
            else
            {
                docType = _app.Core.DocumentTypes.Find(dt);
            }
            if (docType == null)
            {
                return DisconnectAndReturnErrorStream(_app, "Invalid DocumentType: " + dt, HttpStatusCode.BadRequest);
            }
            docQuery.AddDocumentType(docType);

            // Ensure the Keywords exist for the given document, check both ID & Name as above
            foreach (KeyValuePair<string, string> keyMap in keywordMapping)
            {
                KeywordType keyType = null;
                long ktId = 0;
                if (long.TryParse(keyMap.Key, out ktId))
                {
                    keyType = docType.KeywordRecordTypes.FindKeywordType(ktId);
                }
                else
                {
                    keyType = docType.KeywordRecordTypes.FindKeywordType(keyMap.Key);
                }
                if (keyType == null)
                {
                    return DisconnectAndReturnErrorStream(_app, "Invalid KeywordType: " + keyMap.Key, HttpStatusCode.BadRequest);               
                }

                // Throw switch in try/catch
                try
                {
                    Hyland.Unity.Keyword kw = null;
                    switch (keyType.DataType)
                    {
                        case KeywordDataType.Numeric9:
                        case KeywordDataType.Numeric20:
                            kw = keyType.CreateKeyword(long.Parse(keyMap.Value));
                            break;
                        case KeywordDataType.Date:
                        case KeywordDataType.DateTime:
                            // date format is month day year e.g. July 2nd 1980 would be: 07021987
                            if(keyMap.Value.Length != 8) { break; }

                            int year = int.Parse(keyMap.Value.Substring(4)); 
                            int month = int.Parse(keyMap.Value.Substring(0, 2)); 
                            int day = int.Parse(keyMap.Value.Substring(2, 2)); 

                            kw = keyType.CreateKeyword(new DateTime(year, month, day)); 
                            break;
                        case KeywordDataType.Currency:
                        case KeywordDataType.SpecificCurrency:
                            kw = keyType.CreateKeyword(Decimal.Parse(keyMap.Value));
                            break;
                        case KeywordDataType.FloatingPoint:
                            kw = keyType.CreateKeyword(Double.Parse(keyMap.Value));
                            break;
                        case KeywordDataType.AlphaNumeric:
                            kw = keyType.CreateKeyword(keyMap.Value);
                            break;
                        default:
                            break;
                    }
                    // If we succesfully created the keyword based on the supplied key/value, add it to the query
                    if (kw != null)
                    {
                        docQuery.AddKeyword(kw);
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                catch (Exception)
                {
                    return DisconnectAndReturnErrorStream(_app, "Failed to parse Keyword Key/Value: " + keyMap.Key + " / " + keyMap.Value, HttpStatusCode.BadRequest);
                }
            }

            // Execute the query
            DocumentList docList = docQuery.Execute(1000);

            // Ensure only a single document is returned
            if (docList.Count == 0)
            {
                return DisconnectAndReturnErrorStream(_app, "No Documents Returned", HttpStatusCode.ServiceUnavailable);
            }
            else if (docList.Count > 1)
            {
                return DisconnectAndReturnErrorStream(_app, "Multiple Documents Returned -- Only Single Document Retrieval Supported", HttpStatusCode.BadRequest);
            }

            // Grab the one returned document and get its latest revision
            Rendition docRendition = docList[0].DefaultRenditionOfLatestRevision;

            // Set up a DataProvider, get the document's current rendition and grab its Stream
            DefaultDataProvider ddp = _app.Core.Retrieval.Default;
            PageData docData = ddp.GetDocument(docRendition);
            Stream docStream = docData.Stream;
            
            // Copy over the OnBase DocStream over to a memory stream and reset its position to the beginning of the stream 
            MemoryStream ms = new MemoryStream();
            docStream.CopyTo(ms);
            ms.Position = 0;

            // Grab some metadata for the headers
            string docName = docList[0].Name;
            string docExtension = docData.Extension;
            long onBaseFileTypeID = docRendition.FileType.ID;
            string mimeType = docRendition.FileType.MimeType;

            // Cleanup resources
            _app.Disconnect();
            if (_app.IsConnected)
            {
                return DisconnectAndReturnErrorStream(_app, "didn't disconnect", HttpStatusCode.OK);
            }
            docData.Dispose();


            // Write Header/Response info
            String header = "attachment; filename=\"" + docName + "." + docExtension + "\"";
            WebOperationContext.Current.OutgoingResponse.Headers["Content-Disposition"] = header;
            WebOperationContext.Current.OutgoingResponse.Headers["Content-Length"] = ms.Length.ToString();
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/octet-stream"; //docRendition.FileType.MimeType;
            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
            
            // Return the stream
            return ms;
        }
        /// <summary>
        /// Disconnects from the provided app session (if !null/valid), creates a Stream from the supplied error text, and sets the ResponseStatus
        /// </summary>
        /// <param name="_app"></param>
        /// <param name="error"></param>
        /// <param name="statusCode"></param>
        /// <returns>Stream</returns>
        private Stream DisconnectAndReturnErrorStream(Application _app, string error, HttpStatusCode statusCode)
        {
            // Disconnect from the app
            if (_app != null && _app.IsConnected)
            {
                _app.Disconnect();
            }

            // Create, write and reset error stream
            Stream errorStream = new MemoryStream();
            StreamWriter writer = new StreamWriter(errorStream);
            writer.WriteLine(error);
            writer.Flush();
            errorStream.Seek(0, SeekOrigin.Begin);

            // Set response attrs
            WebOperationContext.Current.OutgoingResponse.StatusCode = statusCode;
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
            
            return errorStream;
        }
    }
}